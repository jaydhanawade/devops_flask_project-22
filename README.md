# Devops_flask_project-22

Devops_flask_project-22

Develop a CI/CD Pipeline to Deploy a Web Application
to Store, Upload and Download documents on AWS

Repository Name : Devops_flask_project-22

Business Use Case:
Store & Read Confidential Documents as well as
To Automate Web API Updates or Fix Bugs.
Run/ Execute Project by :

PROJECT DESCRIPTION:
We have developed a CI/CD pipeline to deploy a web application to store, upload documents on AWS. Created a web api in python, By using the docker we build a docker images and pushed it on docker hub registry and then deploy it on Elastic Container Service (ECS) of aws. The whole process is automated by git lab CI/CD pipeline and we have automated the web api updates too. We have also used amazon RDS service to store user data and S3 to store documents.

#CI/CD Pipeline : continuous integration and continuous delivery

devops
.yml
stages:

build
test
deploy

build:
tags:
- group22
script:
- echo "logging in..."
- docker login -u ${dockerID} -p ${dockerPASSWORD} docker.io
- echo "logged in succesfully"
- docker build -t ${username}/project22:recent .
- echo "build the images"
- docker image push ${username}/project22:recent
- echo "images pushed succesfully"
deploy:
tags:
- group22
script:
- aws configure set region us-east-1
- echo "configuration succedded"
- aws ecs create-cluster --cluster-name fargate-cluster
- echo "ecs cluster created.."
- aws ecs register-task-definition --cli-input-json file://taskdefination.json
- echo " task defination configured"
job_1:
stage: deploy
tags:
- group22
script:
- aws ecs create-service --cluster fargate-cluster --service-name fargate-service --task-definition flask --desired-count 1 --launch-type "FARGATE" --network-configuration "awsvpcConfiguration={subnets=[subnet-068e0c8c8c69291fb], securityGroups=[sg-0e03adcf2ffc0efc2],assignPublicIp=ENABLED}"
- echo "Run a script that results in exit code 1. This job fails."
- exit 1
allow_failure: true
job_2:
stage: deploy
tags:
- group22
script:
- aws ecs update-service --cluster fargate-cluster --service fargate-service --force-new-deployment

#SQl
.sql
create database if not exists project default character set utf8 collate utf8_general_ci;
create table if not exists accounts(
id int not null auto_increment,
username varchar(50) not null,
password varchar(50) not null,
email varchar(50) not null,
organisation varchar(50) not null,
address varchar(50) not null,
city varchar(50) not null,
state varchar(50) not null,
country varchar(50) not null,
postalcode int not null,
primary key(id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
db instance identifier -database-1
username-admin
password -12345678
Create table savefile(id int(5) auto_increment , username varchar(500), accid int(5), filesname varchar(500), primary key(id));

Architecture Photo

#BUILT WITH:
AMAZON WEB SERVICES - Cloud service used
HTML, CSS - Scripting language
PYTHON - Programming language to api
GitLab - To Configure Pipeline
Docker Hub - Finding and sharing container images with your team

#Acknowledgments
Guided By : Pradeep Tripathi Sir
Guided By : Kiran Kharat Sir
Guided By : Azam Khan Sir

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jaydhanawade/devops_flask_project-22.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jaydhanawade/devops_flask_project-22/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
